﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oculus
{
    public partial class LoadForm : Form
    {
        Form temporary;
        public LoadForm(int rowsAmount, Form temporary)
        {
            InitializeComponent();
            if (rowsAmount != 0)
            {
                this.temporary = temporary;
                temporary.Enabled = false;
                progressBar1.Maximum = rowsAmount;
            }
            else
                Dispose();

        }
        public void Plus()
        {
            ++progressBar1.Value;
            if (progressBar1.Value == progressBar1.Maximum)
            {
                temporary.Enabled = true;
                Dispose();
                Close();
            }
        }
    }
}
