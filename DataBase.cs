﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Oculus
{
    public class DataBase
    {
        private readonly string connectionString;
        private SqlConnection connection;
        public DataBase(string connectionString)
        {
            this.connectionString = connectionString;

        }
        // для запросов с одним полем
        public List<string> MakeSelect(string query)
        {
            List<string> result = new List<string>();
            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataReader reader = command.ExecuteReader();
                command.Dispose();
                while (reader.Read())
                    result.Add(reader[0].ToString());
                connection.Close();
                reader.Close();
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            
        }
        // для запросов с несколькими полями надо указывать количество полей :)
        public List<string>[] MakeSelect(string query, int colAmount)
        {
            List<string>[] result = new List<string>[colAmount];
            for (int i = 0; i < colAmount; i++)
                result[i] = new List<string>();
            connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand(query, connection);
            SqlDataReader reader = command.ExecuteReader();
            command.Dispose();
            while (reader.Read())
            {
                for(int i=0;i<colAmount;i++)
                {
                    result[i].Add(reader[i].ToString());
                }
            }

            connection.Close();
            reader.Close();
            return result;
        }
        //любые ихменения в таблице
        private int MakeAnyChanges(string query)
        {
            connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                return command.ExecuteNonQuery();
            }
            finally
            {
                try
                {
                    connection.Close();
                }
                catch
                {

                }
                
            }
            
        }
        // далее идут методы для вставки/удаления/апдейта таблицы, по сути все они просто вызывают метод MakeAnyChanges
        public int MakeInsert(string query)
        {
            return MakeAnyChanges(query);
        }
        public int MakeUpdate(string query)
        {
            return MakeAnyChanges(query);
        }
        public int MakeDelete(string query)
        {
            return MakeAnyChanges(query);
        }

        public void GenerateRandomDB()
        {
            // сначала всю базу почистим
            DataBase dataBase1 = new DataBase(connectionString);
            dataBase1.MakeDelete("DELETE FROM Diagnos;");
            dataBase1.MakeDelete("DELETE FROM Complaints;");
            dataBase1.MakeDelete("DELETE FROM Patients;");
            // начальные значения всего
            Random random = new Random();
            DataBase dataBase = new DataBase(connectionString);
            int ID = 0;
            int AppID = 0;

            for (int i=0;i<1500000;i++)
            {
                // Patients
                string Fullname = RandomString();
                string BirthDate = RandomTSQLDate();
                string Phone = RandomPhone();
                string Adress = RandomString();
                dataBase.MakeInsert($"INSERT INTO Patients (ID, FullName, BirthDate,Phone,Adress) VALUES ({ID},N'{Fullname}','{BirthDate}','{Phone}',N'{Adress}');");
                // генерируем от 1 до 8 посещений пациента
                for(int k=0;k<random.Next(1,8);k++)
                {
                    // Complaints
                    string Complaints = RandomString();
                    string Anames = RandomString();
                    string SomaStatus = RandomString();
                    string AppDate = RandomTSQLDate();
                    dataBase.MakeInsert($"INSERT INTO Complaints (Complaints,Anames,SomaStatus,AppDate,AppID,PatientID) VALUES (N'{Complaints}',N'{Anames}',N'{SomaStatus}','{AppDate}',{AppID},{ID});");
                    // Diagnos
                    string Diagnos = RandomString();
                    string OD = RandomOD();
                    string OS = RandomOS();
                    double ODEye = random.Next(50,150);
                    double OSEye = random.Next(50, 150);
                    string ThreatPlan = RandomString();
                    string Appointment = RandomString();
                    dataBase.MakeInsert($"INSERT INTO Diagnos(Diagnos,OD,OS,ODEye,OSEye,ThreatPlan,Appointments,AppID) VALUES (N'{Diagnos}','{OD}','{OS}',{ODEye}, {OSEye},N'{ThreatPlan}',N'{Appointment}', {AppID});");
                    AppID++;
                }
                ID++;
                connection.Close();
            } 
        }
        private static string RandomString()
        {
            string RandomString=String.Empty;
            char c;
            Random random = new Random();
            for (int i=0;i<25;i++)
            {
                c = (char)random.Next(65,90);
                RandomString += c;
            }
            return RandomString;
        }
        private static string RandomPhone()
        {
            string RandomNumber = "(";
            Random random = new Random();
            RandomNumber += random.Next(100, 999);
            RandomNumber += ")-";
            RandomNumber += random.Next(100, 999);
            RandomNumber += "-";
            RandomNumber += random.Next(10, 99);
            RandomNumber += "-";
            RandomNumber += random.Next(10, 99);
            return RandomNumber;
        }
        private static string RandomOD()
        {
            
            string RandomOD = "OD sph ";
            Random random = new Random();
            RandomOD += random.Next(10, 99);
            RandomOD += ",";
            RandomOD += random.Next(10, 99);
            RandomOD += " cyl ";
            RandomOD += random.Next(10, 99);
            RandomOD += ",";
            RandomOD += random.Next(10, 99);
            RandomOD += " ax ";
            RandomOD += random.Next(100, 999);
            return RandomOD;
        }
        private static string RandomOS()
        {
            string RandomOS = "OS sph ";
            Random random = new Random();
            RandomOS += random.Next(10, 99);
            RandomOS += ",";
            RandomOS += random.Next(10, 99);
            RandomOS += " cyl ";
            RandomOS += random.Next(10, 99);
            RandomOS += ",";
            RandomOS += random.Next(10, 99);
            RandomOS += " ax ";
            RandomOS += random.Next(100, 999);
            return RandomOS;
        }
        private static string RandomTSQLDate()
        {
            string RandomTSQLDate = String.Empty;
            Random random = new Random();
            RandomTSQLDate += random.Next(1980, 2019);
            RandomTSQLDate += "-";
            RandomTSQLDate += random.Next(10, 12);
            RandomTSQLDate += "-";
            RandomTSQLDate += random.Next(10, 25);
            return RandomTSQLDate;
        }
    }
}
