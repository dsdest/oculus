﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Oculus
{
    public partial class ClientForm : Form
    {
        private readonly MainForm temporary;
        // reason - причина, по которой вызвали форму, в зависимости от причины меняются поля формы
        // 1 - добавление нового пациента - все поля пустые и доступны
        // 2 - добавление посещения выбранному пациенты - поля ФИО, телефон, дата рождения и адрес недоступны и заполнены, остальное открыто и пустые
        // 3 - редактирование информации о пациенте - видны только предварительно заполненные поля ФИО, дата рождения, адрес и телефон, остальное вообще скрыто и форма уменьшена
        private readonly List<Control> personalInfo, sicknessInfo; // два списка элементов контроля, один с персональной инфой, другой - с инфой по болезни, дате посещения, диагнозу и т.д.
        private readonly byte reason;
        private readonly string ConnectionString;
        // для анимации по таймеру (типа элемент красным подсвечивается, если что-то не заполнено)
        int R = 255;
        int G = 255;
        int B = 255;
        Color color;
        bool doOnce = false;
        List<Control> unfilledElement = new List<Control>();
        // нужно сохранить имя для одного из запросов
        string OldName;

        public ClientForm(MainForm temporary, byte reason, Dictionary<string, string> initValues, string ConnectionString)
        {
            InitializeComponent();
            this.temporary = temporary;
            this.reason = reason;
            this.ConnectionString = ConnectionString;
            temporary.Enabled = false;
            // список с персональными данными
            personalInfo = new List<Control>
            {
                FullName, Phone, BirthDate, Adress
            };
            // список с инфой про болезнь
            sicknessInfo = new List<Control>
            {
                Complaints,Anames,SomaStatus,Diagnos,OD,OS,ODEye,OSEye,TreatmentPlan, Appointments
            };
            switch (reason)
            {
                case 1:
                    {
                        this.Text = "Добавление нового пациента";
                        break;
                    }
                case 2:
                    {
                        this.Text = "Добавление нового посещения пациента";
                        foreach (var ce in personalInfo)
                        {
                            ce.Enabled = false;
                            ce.Text = initValues[ce.Name];
                        }

                        break;
                    }
                case 3:
                    {
                        this.Text = "Редактировать персональную информацию о пациенте";
                        foreach (var ce in sicknessInfo)
                            ce.Visible = false;
                        ComplaintsGroup.Visible = false;
                        // переносим кнопочки повыше и уменьшаем высоту формы
                        int newY = ConfirmButton.Location.Y - 450, newX = ConfirmButton.Location.X;
                        ConfirmButton.Location = new System.Drawing.Point(newX, newY);
                        newY = AbortButton.Location.Y - 450;
                        newX = AbortButton.Location.X;
                        AbortButton.Location = new Point(newX, newY);
                        this.Height = 250;
                        foreach (var ce in personalInfo)
                        {
                            ce.Text = initValues[ce.Name];
                            OldName = FullName.Text;
                        }
                        break;
                    }
            }
        }
        // чтобы не думать, вводить в строку OSEye и ODEye дробное значение через точку или через запятую
        // создаётся эта функция, она будет править точки на запятые при наличии точек
        // и ещё она убирает пробелы
        private string DoubleStringFixer(string doubleNumber)
        {
            if (doubleNumber.Contains("."))
                doubleNumber = doubleNumber.Replace(".", ",");
            if (doubleNumber.Contains(" "))
                doubleNumber = doubleNumber.Replace(" ", "");
            return doubleNumber;
        }
        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            unfilledElement.Clear();
            // для начала проверим персональную инфу
            // отдельно проверка календаря и масктекстбокса
            // проверяется при reason=1 или 3, так как при reason=2 они уже должны быть заполнены
            // если что-то не так заполнено, оно попадает в список, и мигает красным
            if (reason==1 || reason == 3)
            {
                if (!Regex.IsMatch(Phone.Text, @"\(\d\d\d\)-\d\d\d-\d\d-\d\d"))
                {
                    unfilledElement.Add(Phone);
                    timer1.Start();
                }
                if (BirthDate.Value >= DateTime.Now)
                {
                    unfilledElement.Add(label2); // увы, у календаря нет свойства BackColor, буду подсвечивать соответствующий ему лейбл
                    timer1.Start();
                }
                foreach (var ce in personalInfo)
                {
                    if (String.IsNullOrEmpty(ce.Text))
                    {
                        unfilledElement.Add(ce);
                        timer1.Start();
                    }
                }
            }
            // проверим теперь инфу по болезни
            // проверяется при reason=1 или 2, так как при reason=3 редактируется только карточка пациента, а не болезнь
            if(reason==1 || reason == 2)
            {
                if (AppDate.Value >= DateTime.Now)
                {
                    unfilledElement.Add(label7); // увы, у календаря нет свойства BackColor, буду подсвечивать соответствующий ему лейбл
                    timer1.Start();
                }   
                if (!Regex.IsMatch(OD.Text, @"O\w sph \d\d,\d\d cyl \d\d,\d\d ax \d\d\d"))
                {
                    unfilledElement.Add(OD);
                    timer1.Start();
                }
                if (!Regex.IsMatch(OS.Text, @"O\w sph \d\d,\d\d cyl \d\d,\d\d ax \d\d\d"))
                {
                    unfilledElement.Add(OS);
                    timer1.Start();
                }
                try
                {
                    ODEye.Text = DoubleStringFixer(ODEye.Text);
                    var d = Convert.ToDouble(ODEye.Text);
                }
                catch
                {
                    unfilledElement.Add(ODEye);
                    timer1.Start();
                }

                try
                {
                    OSEye.Text = DoubleStringFixer(OSEye.Text);
                    var d = Convert.ToDouble(OSEye.Text);
                }
                catch
                {
                    unfilledElement.Add(OSEye);
                    timer1.Start();
                }

                foreach (var ce in sicknessInfo)
                {
                    if (String.IsNullOrEmpty(ce.Text))
                    {
                        unfilledElement.Add(ce);
                        timer1.Start();
                    }
                }
            }
            // при reason=1 проходят обе проверки, 2 и 3 разделяются

            // теперь проверим, если ли что-то в списке, а если нет, будем забивать запись в БД (в зависимости от кода reason)
            if (unfilledElement.Count == 0)
            {
                // открываем базу
                DataBase dataBase = new DataBase(ConnectionString);
                switch (reason)
                {
                    // код 1: создаём полностью нового клиента, добавляем записи во все таблицы
                    case 1:
                        {
                            // ставятся значения по умолчанию для новой базы
                            int newID = 0;
                            int newAppID = 0;

                            // сначала найдём самые большие ID пациента и AppID и прибавим к ним 1, чтобы использовать при вставке
                            var temp = dataBase.MakeSelect("SELECT MAX(ID) FROM Patients;");
                            if (dataBase.MakeSelect("SELECT count(*) FROM Patients;")[0] != "0")
                                newID = Convert.ToInt32(temp[0]) + 1;

                            temp = dataBase.MakeSelect("SELECT MAX(AppID) FROM Complaints;");
                            if (dataBase.MakeSelect("SELECT count(*) FROM Complaints;")[0] != "0")
                                newAppID = Convert.ToInt32(temp[0])+1;
                            // вставляем в таблицу Patients информацию о самом пациенте
                            dataBase.MakeInsert(PatientInsert(newID));
                            // вставляем в таблицу Complaints информацию о жалобах, сомастатусе и т.д.
                            dataBase.MakeInsert(ComplaintsInsert(newID, newAppID));
                            // вставляем в Diagnos всё по диагнозу
                            DiagnosInsert(newAppID);
                            break;
                        }
                    // код 2: добавляем информацию о посещении уже существующему пациенту
                    case 2:
                        {
                            // добавляем только посещение, не меняя patientID
                            // надо вычислить новый AppID это +1 к старому
                            var temp = dataBase.MakeSelect("SELECT MAX(AppID) FROM Complaints;");
                            int newAppID = Convert.ToInt32(temp[0])+1;
                            // а ID пациента возьму старый
                            temp = dataBase.MakeSelect($"SELECT ID FROM Patients WHERE FullName=N'{FullName.Text}';");
                            int ID = Convert.ToInt32(temp[0]);
                            // вставляем в таблицу Complaints информацию о жалобах, сомастатусе и т.д.
                            dataBase.MakeInsert(ComplaintsInsert(ID,newAppID));
                            // вставляем в Diagnos всё по диагнозу
                            DiagnosInsert(newAppID);
                            break;
                        }
                    // код 3: меняем информацию о пациенте
                    case 3:
                        {
                            // берём старый ID пациента, будем ориентироваться по нему
                            var temp = dataBase.MakeSelect($"SELECT ID FROM Patients WHERE FullName=N'{OldName}';");
                            int ID = Convert.ToInt32(temp[0]);
                            // запрос на обновление
                            string patientUpdate = $"UPDATE Patients SET FullName=N'{FullName.Text}',BirthDate='{DateHandler.Date_ToTSQLStringDate(BirthDate.Value)}',Phone='{Phone.Text}',Adress=N'{Adress.Text}' WHERE ID={ID};";
                            dataBase.MakeUpdate(patientUpdate);
                            break;
                        }
                }
                this.Close();
                this.Dispose();
            }
        }

        private string PatientInsert(int newID)
        {
            string patientInsertString = "INSERT INTO Patients (ID, FullName, BirthDate, Phone, Adress) " +
                                            "VALUES (" + newID + ",N'" + FullName.Text + "','" + DateHandler.Date_ToTSQLStringDate(BirthDate.Value) + "'," +
                                            "'" + Phone.Text + "',N'" + Adress.Text + "');";
            return patientInsertString;
        }

        private string ComplaintsInsert(int newID, int newAppID)
        {
            string complaintsInsertString = "INSERT INTO Complaints (Complaints, Anames,SomaStatus,AppDate,AppID,PatientID) " +
                "VALUES (N'" + Complaints.Text + "',N'" + Anames.Text + "',N'" + SomaStatus.Text + "','" +
                DateHandler.Date_ToTSQLStringDate(AppDate.Value) + "'," + newAppID + "," + newID + ");";
            return complaintsInsertString;
        }
        // используется параметризированный запрос, так как во вставке есть тип Double
        // поэтому тут отдельно от класса это делается
        private void DiagnosInsert(int newAppID)
        {
            double ODEyeValue = Convert.ToDouble(ODEye.Text);
            double OSEyeValue = Convert.ToDouble(OSEye.Text);

            using (SqlConnection sqlConnection = new SqlConnection(ConnectionString))
            {
                string diagnosInsertString = "INSERT INTO Diagnos (Diagnos,OD,OS,ODEye,OSEye,ThreatPlan,Appointments,AppID) VALUES " +
                "(N'" + Diagnos.Text + "','" + OD.Text + "','" + OS.Text + "',@ODEyeValue,@OSEyeValue,N'" + TreatmentPlan.Text + "'," +
                "N'" + Appointments.Text + "'," + newAppID + ");";

                SqlCommand comm = new SqlCommand(diagnosInsertString, sqlConnection);
                comm.Parameters.AddWithValue("@ODEyeValue", ODEyeValue);
                comm.Parameters.AddWithValue("@OSEyeValue", OSEyeValue);
                sqlConnection.Open();
                comm.ExecuteNonQuery();
                comm.Dispose();
            }


            //string diagnosInsertString = "INSERT INTO Diagnos (Diagnos,OD,OS,ODEye,OSEye,ThreatPlan,Appointments,AppID) VALUES " +
            //    "(N'" + Diagnos.Text + "','" + OD.Text + "','" + OS.Text + "'," + ODEye.Text + "," + OSEye.Text + ",N'" + TreatmentPlan.Text + "'," +
            //    "N'" + Appointments.Text + "'," + newAppID + ");";
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
                if (G > 0 && B > 0 && !doOnce)
                {
                    G -= 15;
                    B -= 15;
                    color = Color.FromArgb(R, G, B);
                foreach(var UE in unfilledElement)
                    UE.BackColor = color;
                    if (G == 0 && B == 0)
                        doOnce = true;
                }
                else if (G < 255 && B < 255)
                {
                    G += 15;
                    B += 15;
                    color = Color.FromArgb(R, G, B);
                foreach (var UE in unfilledElement)
                    UE.BackColor = color;
                }
                else
                {
                    timer1.Stop();
                    doOnce = false;
                }            
        }
        private void ClientForm_FormClosing(object sender, FormClosedEventArgs e)
        {
            temporary.Enabled = true;
            temporary.LoadPatientList();
        }

        
    }
}
