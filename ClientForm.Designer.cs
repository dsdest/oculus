﻿namespace Oculus
{
    partial class ClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.FullName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.BirthDate = new System.Windows.Forms.DateTimePicker();
            this.Phone = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ComplaintsGroup = new System.Windows.Forms.GroupBox();
            this.AppDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.SomaStatus = new System.Windows.Forms.RichTextBox();
            this.Anames = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Complaints = new System.Windows.Forms.RichTextBox();
            this.DiagnosGroup = new System.Windows.Forms.GroupBox();
            this.OSEye = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ODEye = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.OS = new System.Windows.Forms.MaskedTextBox();
            this.Diagnos = new System.Windows.Forms.RichTextBox();
            this.OD = new System.Windows.Forms.MaskedTextBox();
            this.Appointments = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TreatmentPlan = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Adress = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ConfirmButton = new System.Windows.Forms.Button();
            this.AbortButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.ComplaintsGroup.SuspendLayout();
            this.DiagnosGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ФИО";
            // 
            // FullName
            // 
            this.FullName.Location = new System.Drawing.Point(52, 6);
            this.FullName.Name = "FullName";
            this.FullName.Size = new System.Drawing.Size(255, 20);
            this.FullName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Дата рождения";
            // 
            // BirthDate
            // 
            this.BirthDate.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.BirthDate.CalendarTitleForeColor = System.Drawing.SystemColors.ButtonFace;
            this.BirthDate.Location = new System.Drawing.Point(107, 36);
            this.BirthDate.Name = "BirthDate";
            this.BirthDate.Size = new System.Drawing.Size(133, 20);
            this.BirthDate.TabIndex = 3;
            // 
            // Phone
            // 
            this.Phone.Location = new System.Drawing.Point(371, 6);
            this.Phone.Mask = "(999)-999-99-99";
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(98, 20);
            this.Phone.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(313, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Телефон";
            // 
            // ComplaintsGroup
            // 
            this.ComplaintsGroup.Controls.Add(this.AppDate);
            this.ComplaintsGroup.Controls.Add(this.label7);
            this.ComplaintsGroup.Controls.Add(this.SomaStatus);
            this.ComplaintsGroup.Controls.Add(this.Anames);
            this.ComplaintsGroup.Controls.Add(this.label6);
            this.ComplaintsGroup.Controls.Add(this.label5);
            this.ComplaintsGroup.Controls.Add(this.Complaints);
            this.ComplaintsGroup.Location = new System.Drawing.Point(9, 131);
            this.ComplaintsGroup.Name = "ComplaintsGroup";
            this.ComplaintsGroup.Size = new System.Drawing.Size(478, 207);
            this.ComplaintsGroup.TabIndex = 8;
            this.ComplaintsGroup.TabStop = false;
            this.ComplaintsGroup.Text = "Жалобы";
            // 
            // AppDate
            // 
            this.AppDate.Location = new System.Drawing.Point(103, 174);
            this.AppDate.Name = "AppDate";
            this.AppDate.Size = new System.Drawing.Size(128, 20);
            this.AppDate.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Дата обращения";
            // 
            // SomaStatus
            // 
            this.SomaStatus.Location = new System.Drawing.Point(244, 103);
            this.SomaStatus.Name = "SomaStatus";
            this.SomaStatus.Size = new System.Drawing.Size(219, 65);
            this.SomaStatus.TabIndex = 11;
            this.SomaStatus.Text = "";
            // 
            // Anames
            // 
            this.Anames.Location = new System.Drawing.Point(6, 103);
            this.Anames.Name = "Anames";
            this.Anames.Size = new System.Drawing.Size(225, 65);
            this.Anames.TabIndex = 10;
            this.Anames.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(267, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(196, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Особенности соматического статуса";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Аллергический анамез";
            // 
            // Complaints
            // 
            this.Complaints.Location = new System.Drawing.Point(6, 19);
            this.Complaints.Name = "Complaints";
            this.Complaints.Size = new System.Drawing.Size(457, 65);
            this.Complaints.TabIndex = 0;
            this.Complaints.Text = "";
            // 
            // DiagnosGroup
            // 
            this.DiagnosGroup.Controls.Add(this.OSEye);
            this.DiagnosGroup.Controls.Add(this.label9);
            this.DiagnosGroup.Controls.Add(this.ODEye);
            this.DiagnosGroup.Controls.Add(this.label8);
            this.DiagnosGroup.Controls.Add(this.OS);
            this.DiagnosGroup.Controls.Add(this.Diagnos);
            this.DiagnosGroup.Controls.Add(this.OD);
            this.DiagnosGroup.Location = new System.Drawing.Point(9, 344);
            this.DiagnosGroup.Name = "DiagnosGroup";
            this.DiagnosGroup.Size = new System.Drawing.Size(478, 155);
            this.DiagnosGroup.TabIndex = 9;
            this.DiagnosGroup.TabStop = false;
            this.DiagnosGroup.Text = "Диагноз";
            // 
            // OSEye
            // 
            this.OSEye.Location = new System.Drawing.Point(337, 120);
            this.OSEye.Name = "OSEye";
            this.OSEye.Size = new System.Drawing.Size(126, 20);
            this.OSEye.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(240, 123);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "OS глазного дна";
            // 
            // ODEye
            // 
            this.ODEye.Location = new System.Drawing.Point(103, 120);
            this.ODEye.Name = "ODEye";
            this.ODEye.Size = new System.Drawing.Size(126, 20);
            this.ODEye.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 123);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "OD глазного дна";
            // 
            // OS
            // 
            this.OS.Location = new System.Drawing.Point(240, 90);
            this.OS.Mask = "OS sph 00\\,00 cyl 00\\,00 \\ax 000";
            this.OS.Name = "OS";
            this.OS.Size = new System.Drawing.Size(223, 20);
            this.OS.TabIndex = 15;
            // 
            // Diagnos
            // 
            this.Diagnos.Location = new System.Drawing.Point(6, 19);
            this.Diagnos.Name = "Diagnos";
            this.Diagnos.Size = new System.Drawing.Size(457, 65);
            this.Diagnos.TabIndex = 14;
            this.Diagnos.Text = "";
            // 
            // OD
            // 
            this.OD.Location = new System.Drawing.Point(6, 90);
            this.OD.Mask = "OD sph 00\\,00 cyl 00\\,00 \\ax 000";
            this.OD.Name = "OD";
            this.OD.Size = new System.Drawing.Size(223, 20);
            this.OD.TabIndex = 0;
            // 
            // Appointments
            // 
            this.Appointments.Location = new System.Drawing.Point(244, 518);
            this.Appointments.Name = "Appointments";
            this.Appointments.Size = new System.Drawing.Size(225, 65);
            this.Appointments.TabIndex = 21;
            this.Appointments.Text = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(246, 502);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Назначения";
            // 
            // TreatmentPlan
            // 
            this.TreatmentPlan.Location = new System.Drawing.Point(12, 518);
            this.TreatmentPlan.Name = "TreatmentPlan";
            this.TreatmentPlan.Size = new System.Drawing.Size(225, 65);
            this.TreatmentPlan.TabIndex = 19;
            this.TreatmentPlan.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 502);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(161, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "План обследования и лечение";
            // 
            // Adress
            // 
            this.Adress.Location = new System.Drawing.Point(59, 66);
            this.Adress.Name = "Adress";
            this.Adress.Size = new System.Drawing.Size(410, 59);
            this.Adress.TabIndex = 23;
            this.Adress.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Адрес";
            // 
            // ConfirmButton
            // 
            this.ConfirmButton.Location = new System.Drawing.Point(12, 607);
            this.ConfirmButton.Name = "ConfirmButton";
            this.ConfirmButton.Size = new System.Drawing.Size(225, 42);
            this.ConfirmButton.TabIndex = 24;
            this.ConfirmButton.Text = "Подтвердить добавление пациента";
            this.ConfirmButton.UseVisualStyleBackColor = true;
            this.ConfirmButton.Click += new System.EventHandler(this.ConfirmButton_Click);
            // 
            // AbortButton
            // 
            this.AbortButton.Location = new System.Drawing.Point(244, 607);
            this.AbortButton.Name = "AbortButton";
            this.AbortButton.Size = new System.Drawing.Size(225, 42);
            this.AbortButton.TabIndex = 25;
            this.AbortButton.Text = "Очистить всё и начать сначала";
            this.AbortButton.UseVisualStyleBackColor = true;
            // 
            // timer1
            // 
            this.timer1.Interval = 15;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // ClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 657);
            this.Controls.Add(this.AbortButton);
            this.Controls.Add(this.ConfirmButton);
            this.Controls.Add(this.Adress);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Appointments);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TreatmentPlan);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.DiagnosGroup);
            this.Controls.Add(this.ComplaintsGroup);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Phone);
            this.Controls.Add(this.BirthDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.FullName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "ClientForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавить клиента/Добавить посещение/Редактировать клиента";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ClientForm_FormClosing);
            this.ComplaintsGroup.ResumeLayout(false);
            this.ComplaintsGroup.PerformLayout();
            this.DiagnosGroup.ResumeLayout(false);
            this.DiagnosGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FullName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker BirthDate;
        private System.Windows.Forms.MaskedTextBox Phone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox ComplaintsGroup;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox SomaStatus;
        private System.Windows.Forms.RichTextBox Anames;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox Complaints;
        private System.Windows.Forms.GroupBox DiagnosGroup;
        private System.Windows.Forms.TextBox OSEye;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox ODEye;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox OS;
        private System.Windows.Forms.RichTextBox Diagnos;
        private System.Windows.Forms.MaskedTextBox OD;
        private System.Windows.Forms.RichTextBox Appointments;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox TreatmentPlan;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox Adress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker AppDate;
        private System.Windows.Forms.Button ConfirmButton;
        private System.Windows.Forms.Button AbortButton;
        private System.Windows.Forms.Timer timer1;
    }
}