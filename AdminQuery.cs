﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Data.SqlClient;

namespace Oculus
{
    public partial class AdminQuery : Form
    {
        private DataBase dataBase;
        string ConnectionString;
        public AdminQuery(string ConnectionString)
        {
            InitializeComponent();
            this.dataBase = new DataBase(ConnectionString);
            this.ConnectionString = ConnectionString;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string query = Query.Text;
            try
            {
                //var res = dataBase.MakeSelect(Query.Text, 5);
                //foreach(List<string> s in res)
                //{
                //    DataGridViewColumn dataGridViewColumn = new DataGridViewColumn();
                //    dataGridViewColumn.
                //}


                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataReader reader = command.ExecuteReader();
                command.Dispose();
                dataGridView1.DataSource = reader;
                connection.Close();
                reader.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Dispose();
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
