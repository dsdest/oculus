﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Oculus
{
    public partial class MainForm : Form
    {
        public static string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\DsDest\Documents\Repository\C#\Oculus\OculusDB.mdf;Integrated Security=True";
        public DataBase dataBase = new DataBase(ConnectionString);
        // список элементов контроля, которые может понадобится очищать
        public List<Control> conElmsToClean;
        public MainForm()
        {
            InitializeComponent();
            conElmsToClean = new List<Control>
            {
                FullName,BirthDate,Phone, Adress,
                Complaints,Anames,SomaStatus,appDate,
                Diagnos,OD,OS,ODEye,OSEye,TreatmentPlan,Appointments
            };
            adminList = new List<ToolStripMenuItem>
            {
                обновитьToolStripMenuItem,
                выходToolStripMenuItem,
                нагенерироватьБДToolStripMenuItem,
                очиститьБазуПолностьюToolStripMenuItem,
                вернутьсяВРежимПользователяToolStripMenuItem,
                queryToolStripMenuItem
            };
            for (int i = 0; i < conElmsToClean.Count; i++)
                conElmsToClean[i].Enabled = false;
        }



        private void EnableDataFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (EnableDataFilter.Checked)
                DateFilter.Enabled = true;
            else
                DateFilter.Enabled = false;
            RenewList(PatientFilter.Text);
        }
        public void LoadPatientList()
        {
            LoadForm load=null;
            var patList = dataBase.MakeSelect("SELECT FullName FROM Patients ORDER BY 1 ASC");
            try
            {
                load = new LoadForm(patList.Count, this);
                if (!load.IsDisposed)
                    load.Show();
            }
            catch
            {
                load.Dispose();
                Environment.Exit(-1);
            }
            
            
            
            PatientList.Items.Clear();
            foreach (var s in patList)
            {
                load.Plus();
                PatientList.Items.Add(s);
            }
            if (PatientList.Items.Count > 0)
                PatientList.SelectedIndex = 0;
            else
                for (int i = 0; i < conElmsToClean.Count; i++)
                    conElmsToClean[i].Text = String.Empty;

        }
        private void PatientList_SelectedIndexChanged(object sender, EventArgs e)
        {
            appDate.Items.Clear();
            if (PatientList.SelectedItem != null)
            {
                // для начала выбираются все даты обращения выбранного пациента
                // заполняются поля, не зависящие от даты обращения, а также список дат обращения
                var patAttr = dataBase.MakeSelect("SELECT pat.FullName, pat.BirthDate, pat.Phone, pat.Adress, com.AppDate FROM Complaints com " +
                    "JOIN Patients pat ON com.PatientID=pat.ID WHERE pat.FullName=N'" + PatientList.SelectedItem.ToString() + "';", 5);
                int amountOfApps = patAttr[0].Count - 1;
                // заполняем независимые поля
                FullName.Text = patAttr[0][0].ToString();
                BirthDate.Value = DateHandler.TSQLStringDate_ToDate(patAttr[1][0].ToString());
                Phone.Text = patAttr[2][0].ToString();
                Adress.Text = patAttr[3][0].ToString();
                //заполняем список дат обращения
                for (int i = 0; i < amountOfApps + 1; i++)
                {
                    appDate.Items.Add(DateHandler.TSQLStringDate_ToStringDate(patAttr[4][i]));
                }
                // выбираем самую последнюю дату, тем самым инициируя событие AppDate_SelectedIndexChanged
                // дальнейшее заполнение уже идёт в этом событии
                // при этом независимые поля уже не выбираются там, выбираются и заполняются только те, что зависят от даты обращения!
                appDate.SelectedIndex = amountOfApps;
            }

        }
        private void AppDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            // строку даты в строку, понимаемую T-SQL
            string dateString = DateHandler.StringDate_ToTSQLStringDate(appDate.SelectedItem.ToString());
            // выбираем все поля, которые зависят от даты обращения и имени пациента
            var parAttr = dataBase.MakeSelect("SELECT com.Complaints, com.Anames, com.SomaStatus," +
                "diag.Diagnos, diag.OD, diag.OS, diag.ODEye, diag.OSEye, diag.ThreatPlan, diag.Appointments " +
                "FROM Complaints com " +
                "JOIN Diagnos diag ON com.AppID = diag.AppID " +
                "JOIN Patients pat ON pat.ID = com.PatientID " +
                "WHERE pat.FullName = N'" + FullName.Text + "' AND com.AppDate = '" + dateString + "';", 10);
            // заполняем все оставшиеся поля
            Complaints.Text = parAttr[0][0];
            Anames.Text = parAttr[1][0];
            SomaStatus.Text = parAttr[2][0];
            Diagnos.Text = parAttr[3][0];
            OD.Text = parAttr[4][0];
            OS.Text = parAttr[5][0];
            ODEye.Text = parAttr[6][0];
            OSEye.Text = parAttr[7][0];
            TreatmentPlan.Text = parAttr[8][0];
            Appointments.Text = parAttr[9][0];
        }

        private void PatientFilter_TextChanged(object sender, EventArgs e)
        {
            RenewList(PatientFilter.Text);            
        }

        private void DateFilter_ValueChanged(object sender, EventArgs e)
        {
            RenewList(PatientFilter.Text);
        }

        // функция для обновления списка при любом изменении (включение/выключение фильтра по датам и изменении теста
        private void RenewList(string FullName)
        {
            // для начала почистим все элементы контроля
            foreach (var c in conElmsToClean)
            {
                c.Text = "";
            }
            // выборка, если нет проверки по дате
            if (!EnableDataFilter.Checked)
            {
                var patList = dataBase.MakeSelect("SELECT FullName FROM Patients WHERE FullName LIKE N'%" + FullName + "%'  ORDER BY 1 ASC;");
                PatientList.Items.Clear();
                foreach (var s in patList)
                    PatientList.Items.Add(s);
            }
            //выборка, если есть проверка по дате
            else
            {
                string dateString = DateHandler.Date_ToTSQLStringDate(DateFilter.Value);
                var patList = dataBase.MakeSelect("SELECT pat.FullName FROM Patients pat JOIN Complaints com ON com.PatientID=pat.ID WHERE pat.FullName LIKE N'%" + 
                    PatientFilter.Text + "%' AND com.AppDate='" + dateString + "'  ORDER BY 1 ASC;");
                PatientList.Items.Clear();
                foreach (var s in patList)
                    PatientList.Items.Add(s);
            }
            // выбираем самого верхнего челика из списка, если список не пуст
            if (PatientList.Items.Count>0)
                PatientList.SelectedIndex = 0;
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            LoadPatientList();
        }

        private void ДобавитьНовогоПациентаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var clientForm = new ClientForm(this, 1, null, ConnectionString);
            clientForm.Show();
        }

        private void ДобавитьПосещениеВыбранногоПациентаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> initValues = new Dictionary<string, string>
            {
                { "FullName", FullName.Text },
                { "Phone", Phone.Text },
                { "BirthDate", DateHandler.Date_ToStringDate(BirthDate.Value) },
                { "Adress", Adress.Text }
            };
            var clientForm = new ClientForm(this, 2, initValues, ConnectionString);
            clientForm.Show();
        }

        private void РедактироватьВыбраннуюЗаписьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> initValues = new Dictionary<string, string>
            {
                { "FullName", FullName.Text },
                { "Phone", Phone.Text },
                { "BirthDate", DateHandler.Date_ToStringDate(BirthDate.Value) },
                { "Adress", Adress.Text }
            };
            var clientForm = new ClientForm(this, 3, initValues, ConnectionString);
            clientForm.Show();
        }

        private void МенюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (PatientList.SelectedIndex==-1)
            {
                добавитьПосещениеВыбранногоПациентаToolStripMenuItem.Enabled = false;
                редактироватьДанныеОПациентеToolStripMenuItem.Enabled = false;
                удалитьВыбранногоПациентаToolStripMenuItem.Enabled = false;
            }
            else
            {
                добавитьПосещениеВыбранногоПациентаToolStripMenuItem.Enabled = true;
                редактироватьДанныеОПациентеToolStripMenuItem.Enabled = true;
                удалитьВыбранногоПациентаToolStripMenuItem.Enabled = true;
            }
        }
        private void УдалитьВыбранногоПациентаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Это действие удалит пациента и все его записи из базы. Продолжить?", "Предупреждение", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.Yes)
            {
                // получаем ID пациента, по которому будем удалять записи
                var temp = dataBase.MakeSelect($"SELECT ID FROM Patients WHERE FullName=N'{FullName.Text}'");
                int ID = Convert.ToInt32(temp[0].ToString());
                // получаем AppID, по которым будем удалять записи
                temp = dataBase.MakeSelect($"SELECT AppID FROM Complaints WHERE PatientID={ID};");
                // так как AppID может быть несколько, перечислим их через запятую, чтобы потом использовать в конструкции IN
                string appIDs = "";
                foreach (var s in temp)
                {
                    appIDs += s + ",";
                }
                appIDs = appIDs.Remove(appIDs.Length - 1);
                string deletePatients = $"DELETE FROM Patients WHERE ID={ID};";
                string deleteDiagnos = $"DELETE FROM Diagnos WHERE AppID IN ({appIDs});";
                string deleteComplaints = $"DELETE FROM Complaints WHERE PatientID={ID};";
                // выполняем запросы на удаление
                dataBase.MakeDelete(deleteDiagnos);
                dataBase.MakeDelete(deleteComplaints);
                dataBase.MakeDelete(deletePatients);
                LoadPatientList();
            }
        }


        // всё что ниже доступно будет при нажатии Alt+F5 и ввода пароля администратора!
        private readonly List<ToolStripMenuItem> adminList;
        private void PatientList_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Alt && e.KeyCode == Keys.F5)
            {
                MessageBox.Show("OH HI THERE! Now you've got it all!", "Greetings, my dear admin ^_^", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                foreach (var s in adminList)
                {
                    s.Visible = true;
                }

            }
        }

        private void ОбновитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadPatientList();
        }

        private void НагенерироватьБДToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                dataBase.GenerateRandomDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ОчиститьБазуПолностьюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PatientList.SelectedIndex = -1;
            dataBase.MakeDelete("DELETE FROM Diagnos;");
            dataBase.MakeDelete("DELETE FROM Complaints;");
            dataBase.MakeDelete("DELETE FROM Patients;");
            LoadPatientList();
        }
        private void ВыходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void ВернутьсяВРежимПользователяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var s in adminList)
            {
                s.Visible = false;
            }
            MessageBox.Show("OH HI THERE! Now you've got standart access!", "Greetings, my dear user ^_^", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void SELECTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var AdminForm = new AdminQuery(ConnectionString);
            AdminForm.Text += "SELECT";
            AdminForm.Show();
        }

        private void INSERTUPDATEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var AdminForm = new AdminQuery(ConnectionString);
            AdminForm.Text += "UPDATE";
            AdminForm.Show();
        }
    }
}
