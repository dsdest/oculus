﻿namespace Oculus
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.менюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьНовогоПациентаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьПосещениеВыбранногоПациентаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.редактироватьДанныеОПациентеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьВыбранногоПациентаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обновитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нагенерироватьБДToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.очиститьБазуПолностьюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вернутьсяВРежимПользователяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.queryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sELECTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iNSERTUPDATEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.EnableDataFilter = new System.Windows.Forms.CheckBox();
            this.DateFilter = new System.Windows.Forms.DateTimePicker();
            this.PatientFilter = new System.Windows.Forms.TextBox();
            this.PatientList = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Phone = new System.Windows.Forms.MaskedTextBox();
            this.Adress = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BirthDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.FullName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Complaints = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.appDate = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SomaStatus = new System.Windows.Forms.RichTextBox();
            this.Anames = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.OSEye = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ODEye = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.OS = new System.Windows.Forms.MaskedTextBox();
            this.Diagnos = new System.Windows.Forms.RichTextBox();
            this.OD = new System.Windows.Forms.MaskedTextBox();
            this.TreatmentPlan = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Appointments = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.менюToolStripMenuItem,
            this.обновитьToolStripMenuItem,
            this.выходToolStripMenuItem,
            this.нагенерироватьБДToolStripMenuItem,
            this.очиститьБазуПолностьюToolStripMenuItem,
            this.вернутьсяВРежимПользователяToolStripMenuItem,
            this.queryToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(824, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // менюToolStripMenuItem
            // 
            this.менюToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьНовогоПациентаToolStripMenuItem,
            this.добавитьПосещениеВыбранногоПациентаToolStripMenuItem,
            this.редактироватьДанныеОПациентеToolStripMenuItem,
            this.удалитьВыбранногоПациентаToolStripMenuItem});
            this.менюToolStripMenuItem.Name = "менюToolStripMenuItem";
            this.менюToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.менюToolStripMenuItem.Text = "Меню";
            this.менюToolStripMenuItem.Click += new System.EventHandler(this.МенюToolStripMenuItem_Click);
            // 
            // добавитьНовогоПациентаToolStripMenuItem
            // 
            this.добавитьНовогоПациентаToolStripMenuItem.Name = "добавитьНовогоПациентаToolStripMenuItem";
            this.добавитьНовогоПациентаToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
            this.добавитьНовогоПациентаToolStripMenuItem.Text = "Добавить нового пациента";
            this.добавитьНовогоПациентаToolStripMenuItem.Click += new System.EventHandler(this.ДобавитьНовогоПациентаToolStripMenuItem_Click);
            // 
            // добавитьПосещениеВыбранногоПациентаToolStripMenuItem
            // 
            this.добавитьПосещениеВыбранногоПациентаToolStripMenuItem.Name = "добавитьПосещениеВыбранногоПациентаToolStripMenuItem";
            this.добавитьПосещениеВыбранногоПациентаToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
            this.добавитьПосещениеВыбранногоПациентаToolStripMenuItem.Text = "Добавить посещение выбранного пациента";
            this.добавитьПосещениеВыбранногоПациентаToolStripMenuItem.Click += new System.EventHandler(this.ДобавитьПосещениеВыбранногоПациентаToolStripMenuItem_Click);
            // 
            // редактироватьДанныеОПациентеToolStripMenuItem
            // 
            this.редактироватьДанныеОПациентеToolStripMenuItem.Name = "редактироватьДанныеОПациентеToolStripMenuItem";
            this.редактироватьДанныеОПациентеToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
            this.редактироватьДанныеОПациентеToolStripMenuItem.Text = "Редактировать данные о пациенте";
            this.редактироватьДанныеОПациентеToolStripMenuItem.Click += new System.EventHandler(this.РедактироватьВыбраннуюЗаписьToolStripMenuItem_Click);
            // 
            // удалитьВыбранногоПациентаToolStripMenuItem
            // 
            this.удалитьВыбранногоПациентаToolStripMenuItem.Name = "удалитьВыбранногоПациентаToolStripMenuItem";
            this.удалитьВыбранногоПациентаToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
            this.удалитьВыбранногоПациентаToolStripMenuItem.Text = "Удалить выбранного пациента";
            this.удалитьВыбранногоПациентаToolStripMenuItem.Click += new System.EventHandler(this.УдалитьВыбранногоПациентаToolStripMenuItem_Click);
            // 
            // обновитьToolStripMenuItem
            // 
            this.обновитьToolStripMenuItem.Name = "обновитьToolStripMenuItem";
            this.обновитьToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.обновитьToolStripMenuItem.Text = "Обновить";
            this.обновитьToolStripMenuItem.Visible = false;
            this.обновитьToolStripMenuItem.Click += new System.EventHandler(this.ОбновитьToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Visible = false;
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.ВыходToolStripMenuItem_Click);
            // 
            // нагенерироватьБДToolStripMenuItem
            // 
            this.нагенерироватьБДToolStripMenuItem.Name = "нагенерироватьБДToolStripMenuItem";
            this.нагенерироватьБДToolStripMenuItem.Size = new System.Drawing.Size(127, 20);
            this.нагенерироватьБДToolStripMenuItem.Text = "Нагенерировать БД";
            this.нагенерироватьБДToolStripMenuItem.Visible = false;
            this.нагенерироватьБДToolStripMenuItem.Click += new System.EventHandler(this.НагенерироватьБДToolStripMenuItem_Click);
            // 
            // очиститьБазуПолностьюToolStripMenuItem
            // 
            this.очиститьБазуПолностьюToolStripMenuItem.Name = "очиститьБазуПолностьюToolStripMenuItem";
            this.очиститьБазуПолностьюToolStripMenuItem.Size = new System.Drawing.Size(163, 20);
            this.очиститьБазуПолностьюToolStripMenuItem.Text = "Очистить базу полностью";
            this.очиститьБазуПолностьюToolStripMenuItem.Visible = false;
            this.очиститьБазуПолностьюToolStripMenuItem.Click += new System.EventHandler(this.ОчиститьБазуПолностьюToolStripMenuItem_Click);
            // 
            // вернутьсяВРежимПользователяToolStripMenuItem
            // 
            this.вернутьсяВРежимПользователяToolStripMenuItem.Name = "вернутьсяВРежимПользователяToolStripMenuItem";
            this.вернутьсяВРежимПользователяToolStripMenuItem.Size = new System.Drawing.Size(203, 20);
            this.вернутьсяВРежимПользователяToolStripMenuItem.Text = "Вернуться в режим пользователя";
            this.вернутьсяВРежимПользователяToolStripMenuItem.Visible = false;
            this.вернутьсяВРежимПользователяToolStripMenuItem.Click += new System.EventHandler(this.ВернутьсяВРежимПользователяToolStripMenuItem_Click);
            // 
            // queryToolStripMenuItem
            // 
            this.queryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sELECTToolStripMenuItem,
            this.iNSERTUPDATEToolStripMenuItem});
            this.queryToolStripMenuItem.Name = "queryToolStripMenuItem";
            this.queryToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.queryToolStripMenuItem.Text = "Query";
            this.queryToolStripMenuItem.Visible = false;
            // 
            // sELECTToolStripMenuItem
            // 
            this.sELECTToolStripMenuItem.Name = "sELECTToolStripMenuItem";
            this.sELECTToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.sELECTToolStripMenuItem.Text = "SELECT";
            this.sELECTToolStripMenuItem.Click += new System.EventHandler(this.SELECTToolStripMenuItem_Click);
            // 
            // iNSERTUPDATEToolStripMenuItem
            // 
            this.iNSERTUPDATEToolStripMenuItem.Name = "iNSERTUPDATEToolStripMenuItem";
            this.iNSERTUPDATEToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.iNSERTUPDATEToolStripMenuItem.Text = "INSERT/UPDATE...";
            this.iNSERTUPDATEToolStripMenuItem.Click += new System.EventHandler(this.INSERTUPDATEToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.EnableDataFilter);
            this.groupBox1.Controls.Add(this.DateFilter);
            this.groupBox1.Controls.Add(this.PatientFilter);
            this.groupBox1.Controls.Add(this.PatientList);
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 609);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Пациенты в базе";
            // 
            // EnableDataFilter
            // 
            this.EnableDataFilter.AutoSize = true;
            this.EnableDataFilter.Location = new System.Drawing.Point(153, 22);
            this.EnableDataFilter.Name = "EnableDataFilter";
            this.EnableDataFilter.Size = new System.Drawing.Size(15, 14);
            this.EnableDataFilter.TabIndex = 3;
            this.EnableDataFilter.UseVisualStyleBackColor = true;
            this.EnableDataFilter.CheckedChanged += new System.EventHandler(this.EnableDataFilter_CheckedChanged);
            // 
            // DateFilter
            // 
            this.DateFilter.Enabled = false;
            this.DateFilter.Location = new System.Drawing.Point(174, 19);
            this.DateFilter.Name = "DateFilter";
            this.DateFilter.Size = new System.Drawing.Size(120, 20);
            this.DateFilter.TabIndex = 2;
            this.DateFilter.ValueChanged += new System.EventHandler(this.DateFilter_ValueChanged);
            // 
            // PatientFilter
            // 
            this.PatientFilter.Location = new System.Drawing.Point(6, 19);
            this.PatientFilter.Name = "PatientFilter";
            this.PatientFilter.Size = new System.Drawing.Size(120, 20);
            this.PatientFilter.TabIndex = 1;
            this.PatientFilter.TextChanged += new System.EventHandler(this.PatientFilter_TextChanged);
            // 
            // PatientList
            // 
            this.PatientList.FormattingEnabled = true;
            this.PatientList.Location = new System.Drawing.Point(6, 51);
            this.PatientList.Name = "PatientList";
            this.PatientList.Size = new System.Drawing.Size(288, 550);
            this.PatientList.TabIndex = 0;
            this.PatientList.SelectedIndexChanged += new System.EventHandler(this.PatientList_SelectedIndexChanged);
            this.PatientList.KeyUp += new System.Windows.Forms.KeyEventHandler(this.PatientList_KeyUp);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Phone);
            this.groupBox2.Controls.Add(this.Adress);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.BirthDate);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.FullName);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(318, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(479, 141);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Персональные данные";
            // 
            // Phone
            // 
            this.Phone.Location = new System.Drawing.Point(9, 104);
            this.Phone.Mask = "(999)-999-99-99";
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(117, 20);
            this.Phone.TabIndex = 8;
            // 
            // Adress
            // 
            this.Adress.Location = new System.Drawing.Point(133, 65);
            this.Adress.Name = "Adress";
            this.Adress.Size = new System.Drawing.Size(331, 59);
            this.Adress.TabIndex = 7;
            this.Adress.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(277, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Адрес";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Телефон";
            // 
            // BirthDate
            // 
            this.BirthDate.Location = new System.Drawing.Point(9, 65);
            this.BirthDate.Name = "BirthDate";
            this.BirthDate.Size = new System.Drawing.Size(117, 20);
            this.BirthDate.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Дата рождения";
            // 
            // FullName
            // 
            this.FullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FullName.ForeColor = System.Drawing.SystemColors.InfoText;
            this.FullName.Location = new System.Drawing.Point(46, 20);
            this.FullName.Name = "FullName";
            this.FullName.Size = new System.Drawing.Size(418, 20);
            this.FullName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ФИО";
            // 
            // Complaints
            // 
            this.Complaints.Location = new System.Drawing.Point(6, 19);
            this.Complaints.Name = "Complaints";
            this.Complaints.Size = new System.Drawing.Size(457, 65);
            this.Complaints.TabIndex = 0;
            this.Complaints.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Аллергический анамез";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.appDate);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.SomaStatus);
            this.groupBox3.Controls.Add(this.Anames);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.Complaints);
            this.groupBox3.Location = new System.Drawing.Point(319, 175);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(478, 201);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Жалобы";
            // 
            // appDate
            // 
            this.appDate.FormattingEnabled = true;
            this.appDate.Location = new System.Drawing.Point(103, 171);
            this.appDate.Name = "appDate";
            this.appDate.Size = new System.Drawing.Size(128, 21);
            this.appDate.TabIndex = 13;
            this.appDate.SelectedIndexChanged += new System.EventHandler(this.AppDate_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Дата обращения";
            // 
            // SomaStatus
            // 
            this.SomaStatus.Location = new System.Drawing.Point(244, 103);
            this.SomaStatus.Name = "SomaStatus";
            this.SomaStatus.Size = new System.Drawing.Size(219, 65);
            this.SomaStatus.TabIndex = 11;
            this.SomaStatus.Text = "";
            // 
            // Anames
            // 
            this.Anames.Location = new System.Drawing.Point(6, 103);
            this.Anames.Name = "Anames";
            this.Anames.Size = new System.Drawing.Size(225, 65);
            this.Anames.TabIndex = 10;
            this.Anames.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(267, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(196, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Особенности соматического статуса";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.OSEye);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.ODEye);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.OS);
            this.groupBox4.Controls.Add(this.Diagnos);
            this.groupBox4.Controls.Add(this.OD);
            this.groupBox4.Location = new System.Drawing.Point(319, 382);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(478, 155);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Диагноз";
            // 
            // OSEye
            // 
            this.OSEye.Location = new System.Drawing.Point(337, 120);
            this.OSEye.Name = "OSEye";
            this.OSEye.Size = new System.Drawing.Size(126, 20);
            this.OSEye.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(240, 123);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "OS глазного дна";
            // 
            // ODEye
            // 
            this.ODEye.Location = new System.Drawing.Point(103, 120);
            this.ODEye.Name = "ODEye";
            this.ODEye.Size = new System.Drawing.Size(126, 20);
            this.ODEye.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 123);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "OD глазного дна";
            // 
            // OS
            // 
            this.OS.Location = new System.Drawing.Point(240, 90);
            this.OS.Mask = "OS sph 00\\,00 cyl 00\\,00 \\ax 000";
            this.OS.Name = "OS";
            this.OS.Size = new System.Drawing.Size(223, 20);
            this.OS.TabIndex = 15;
            // 
            // Diagnos
            // 
            this.Diagnos.Location = new System.Drawing.Point(6, 19);
            this.Diagnos.Name = "Diagnos";
            this.Diagnos.Size = new System.Drawing.Size(457, 65);
            this.Diagnos.TabIndex = 14;
            this.Diagnos.Text = "";
            // 
            // OD
            // 
            this.OD.Location = new System.Drawing.Point(6, 90);
            this.OD.Mask = "OD sph 00\\,00 cyl 00\\,00 \\ax 000";
            this.OD.Name = "OD";
            this.OD.Size = new System.Drawing.Size(223, 20);
            this.OD.TabIndex = 0;
            // 
            // TreatmentPlan
            // 
            this.TreatmentPlan.Location = new System.Drawing.Point(325, 560);
            this.TreatmentPlan.Name = "TreatmentPlan";
            this.TreatmentPlan.Size = new System.Drawing.Size(225, 65);
            this.TreatmentPlan.TabIndex = 15;
            this.TreatmentPlan.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(325, 544);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(161, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "План обследования и лечение";
            // 
            // Appointments
            // 
            this.Appointments.Location = new System.Drawing.Point(557, 560);
            this.Appointments.Name = "Appointments";
            this.Appointments.Size = new System.Drawing.Size(225, 65);
            this.Appointments.TabIndex = 17;
            this.Appointments.Text = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(557, 544);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Назначения";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 641);
            this.Controls.Add(this.Appointments);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TreatmentPlan);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Oculus";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem менюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавитьНовогоПациентаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьВыбранногоПациентаToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem обновитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ListBox PatientList;
        private System.Windows.Forms.TextBox PatientFilter;
        private System.Windows.Forms.DateTimePicker DateFilter;
        private System.Windows.Forms.CheckBox EnableDataFilter;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox FullName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox Adress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker BirthDate;
        private System.Windows.Forms.RichTextBox Complaints;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox SomaStatus;
        private System.Windows.Forms.RichTextBox Anames;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox appDate;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.MaskedTextBox OS;
        private System.Windows.Forms.RichTextBox Diagnos;
        private System.Windows.Forms.MaskedTextBox OD;
        private System.Windows.Forms.MaskedTextBox Phone;
        private System.Windows.Forms.TextBox ODEye;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox OSEye;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox TreatmentPlan;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox Appointments;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ToolStripMenuItem добавитьПосещениеВыбранногоПациентаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нагенерироватьБДToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem редактироватьДанныеОПациентеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem очиститьБазуПолностьюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вернутьсяВРежимПользователяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem queryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sELECTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iNSERTUPDATEToolStripMenuItem;
    }
}

