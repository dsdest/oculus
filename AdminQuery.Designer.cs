﻿namespace Oculus
{
    partial class AdminQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Query = new System.Windows.Forms.RichTextBox();
            this.ExecButton = new System.Windows.Forms.Button();
            this.AbortButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Query
            // 
            this.Query.Location = new System.Drawing.Point(12, 12);
            this.Query.Name = "Query";
            this.Query.Size = new System.Drawing.Size(461, 124);
            this.Query.TabIndex = 0;
            this.Query.Text = "";
            // 
            // ExecButton
            // 
            this.ExecButton.Location = new System.Drawing.Point(12, 142);
            this.ExecButton.Name = "ExecButton";
            this.ExecButton.Size = new System.Drawing.Size(237, 50);
            this.ExecButton.TabIndex = 1;
            this.ExecButton.Text = "Execute";
            this.ExecButton.UseVisualStyleBackColor = true;
            this.ExecButton.Click += new System.EventHandler(this.Button1_Click);
            // 
            // AbortButton
            // 
            this.AbortButton.Location = new System.Drawing.Point(255, 142);
            this.AbortButton.Name = "AbortButton";
            this.AbortButton.Size = new System.Drawing.Size(218, 50);
            this.AbortButton.TabIndex = 2;
            this.AbortButton.Text = "Fuck go back";
            this.AbortButton.UseVisualStyleBackColor = true;
            this.AbortButton.Click += new System.EventHandler(this.Button2_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 198);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(461, 416);
            this.dataGridView1.TabIndex = 3;
            // 
            // AdminQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 629);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.AbortButton);
            this.Controls.Add(this.ExecButton);
            this.Controls.Add(this.Query);
            this.Name = "AdminQuery";
            this.Text = "AdminQuery";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox Query;
        private System.Windows.Forms.Button ExecButton;
        private System.Windows.Forms.Button AbortButton;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}