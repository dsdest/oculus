﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oculus
{
    public static class DateHandler
    {
        // конвертер дат:
        // дата в T-SQL
        // дата в строку
        // строка в дату
        // строка в T-SQL
        // T-SQL в дату
        // T-SQL в строку

        // обозначения: Format1_ToFormat2
        // где Format1 и Format2 могут быть:
        // Date - дата
        // StringDate - строка с датой (в формате ToShortDateString)
        // TSQLStringDate - строка с датой, понятная для T-SQL


        // Дата в T-SQL
        public static string Date_ToTSQLStringDate(DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }

        // Дата в строку
        public static string Date_ToStringDate(DateTime date)
        {
            return date.ToShortDateString();
        }

        // Строка в дату
        public static DateTime StringDate_ToDate(string input)
        {
            DateTime date = new DateTime();
            bool success = DateTime.TryParse(input, out date);
            return success ? date : Convert.ToDateTime(null);
        }

        // Строка в T-SQL
        public static string StringDate_ToTSQLStringDate(string input)
        {
            DateTime date = new DateTime();
            bool success = DateTime.TryParse(input, out date);
            return success ? date.ToString("yyyy-MM-dd") : null;
        }

        // T-SQL в дату
        public static DateTime TSQLStringDate_ToDate(string input)
        {
            DateTime date = new DateTime();
            bool success =  DateTime.TryParse(input, out date);
            return success ? date : Convert.ToDateTime(null);
        }

        // T-SQL в строку
        public static string TSQLStringDate_ToStringDate(string input)
        {
            DateTime dates = Convert.ToDateTime(input);
            return dates.ToShortDateString();
        }
    }
}
